#!bin/bash

$(aws ecr get-login --region us-west-2)
ecr_repo=407156075963.dkr.ecr.us-west-2.amazonaws.com/facebook_app
docker pull ${ecr_repo}:latest
# build docker image with tag name -t facebook-app
# docker file in same directory is used
docker build --no-cache -t facebook-app .
#docker tag -f face-engine-portal:latest ${ecr_repo}:latest
#docker push ${ecr_repo}:latest

docker stop facebook-app
docker rm facebook-app

docker run -d --name facebook-app --restart=always -e NODE_ENV=$1 -p 8080:8080 facebook-app
