var express = require('express');
var router = express.Router();
var request = require('request');
var Buffer = require('buffer');
var json_request = request.defaults({headers: {'content-type': 'application/json'}});
var general = require('../utils/general');
const api_server = general.api_server;
const host_url = general.host_url;
const log_prefix = "[Facebook-App] ";


/* GET home page. */
router.all('/', function(req, res, next) {
  res.redirect('/3');
});

router.post('/vote',function(req,res,next){
  var poll_option_id = req.body.poll_option_id;
  var poll_id = req.body.poll_id;
  var user_id = req.session.user_id;
  console.log("poll_option_id: " + poll_option_id + " poll_id: " + poll_id + " user_id: " + user_id);
  json_request.post(
  {
    url: api_server + '/feedback/vote',
    body: JSON.stringify(
    {
      user_id: user_id,
      feedback_poll_option_id: poll_option_id,
      feedback_poll_id: poll_id
    })
      },function (error, response, body) {
        console.log(body);
        if (response.statusCode == 200) {
          res.end(body);
        } else {
          // TODO handle the error case of betaFace server
          res.end(JSON.stringify(response));
        }
      }
  );
})

router.all('/:poll_id', function(req, res, next) {
    var poll_id = req.params.poll_id;
    json_request.get(
    {
      url: api_server + '/feedback/' + poll_id
    }, function(error, response, body){
      if (response.statusCode == 200) {
        body = JSON.parse(body);
        var meta = {};
        meta.og_image = body.polls[0].thumbnail_url;
        meta.og_site_name = "ButterflyHub";
        meta.og_type = "website";
        meta.og_url = "https://apps.facebook.com/butterflybeauty/" + body.polls[0].id;
        meta.og_description = "Hey there, help " + body.polls[0].user_poll.username + " make better decisions with your vote. Every vote matters!" + " Explore millions of different beauty looks for different occasions on our Butterfly Beauty application on the AppStore. Download now!";
        meta.og_title = body.polls[0].question.question_details;
        meta.fb_app_id = "1055854274498060";
        res.render('index', {meta: meta});
      }
    });
});

router.all('/get_data/:poll_id', function(req, res, next){
  var poll_id = req.params.poll_id;
  if(req.body.fb_users == undefined)
  {
    res.render('index', { title: 'Express' });
  }else
  {
    var fb_users = req.body.fb_users;
    json_request.post(
      {
        url: api_server + '/users/authenticate/facebook',
        body: JSON.stringify(
          {
            fb_user: fb_users
          })
      },
      //main handler function here:
      function (error, response, body) {';'
        if (response.statusCode == 200) {
          if(req.session.user_id == undefined)
              req.session.user_id = JSON.parse(body).data.users[0].user_id;
        json_request.get(
        {
          url: api_server + '/feedback/' + poll_id
        }, function(error, response, body){
          if (response.statusCode == 200) {
            var main = {};
            // JSON.parse(body)[0].user_id = req.session.user_id;
            var data = JSON.parse(body);
            data.user_id = req.session.user_id;
            main["data"] = JSON.stringify(data);
            res.end(JSON.stringify(main));
          }
        });
      }else
      {
          console.log("error when authenticate");
      }
    });
  }
});

router.post('/upload', function(req, res, next){
  console.log("upload");
  console.log(req.body.formdata);
    var img = req.body.formdata;
})

module.exports = router;
