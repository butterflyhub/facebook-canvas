var fb_me;
    window.fbAsyncInit = function() {
      FB.init({
        appId      : '1055854274498060',
        xfbml      : true,
        version    : 'v2.7'
      });
      jQuery(window).load(function(){
        $('.loadgif').css("display","block");
        FB.Canvas.setDoneLoading( function(result) {
            FB.Canvas.setSize({height: $('#wrapper').height() });
        });
      });
    function onLogin(response) {
      if (response.status == 'connected') {
        FB.api('/me', {fields: ['id', 'name', 'first_name', 'last_name', 'email', 'location', 'locale', 'age_range', 'gender', 'birthday']}, function(response){
          fb_me = response;
          Init();
        });
      }
    }

    FB.getLoginStatus(function(response) {
      if (response.status == 'connected') {
        onLogin(response);
      } else {
        FB.login(function(response) {
            if (response.authResponse) {
              onLogin(response);
            } else {
              return false;
            }
        },{scope:'public_profile,user_friends,email'});
      }
    });
  
    function Init()
    {
      var poll_id = window.location.pathname.split('/')[1];
      var url = "/get_data/" + poll_id;
      $.ajax({
        type: "POST",
        url: url,
        contentType: "application/json",
        data: JSON.stringify({"fb_users" : fb_me}),
        dataType: "json",
        async: false,
        success: function (res) {
            $("#main_data").val(res.data);
            LoadData();
            $('.loadgif').css("display","none");
        },
        error: function (error) {
            $('.loadgif').css("display","none");
            alert(error.status + "<--and--> " + error.statusText);
        }
      });
    }
    function LoadData()
    {
      var data = $("#main_data").val();
      var item = JSON.parse(data).polls[0];
      // Load data non-mobile
      var userpoll = item.user_poll.username != "" ? item.user_poll.username : item.user_poll.firstname + " " +   item.user_poll.lastname;
      $(".question-title").text(userpoll + " asks:");
      $(".headertop .titlepage").text(item.question.question_details);
      $(".question-detail").text(item.question.question_details);
      $(".likepage .titlepage").text(item.question.question_details);
      $(".likepagemobile .titlepage").text(item.question.question_details);
      var poll_1 = "<img class='square' src='"+item.option[0].image_url+"' id='"+item.option[0].id+"' />";
      var poll_2 = "<img class='square' src='"+item.option[1].image_url+"' id='"+item.option[1].id+"' />";
      $(".contentlp .bannercontent1").html(poll_1);
      $(".contentlp .bannercontent3").html(poll_2);
      var animation_poll = '<video width="100%" height="220" autoplay loop>';
          animation_poll += '<source src="'+item.morphing_url+'" type="video/mp4">';
          animation_poll += 'Your browser does not support HTML5 video.';
          animation_poll += '</video>';
      $(".contentlp .animation").html(animation_poll);
      $(".contentlp .bg_text_hand").text(item.score + '% match');
      // Bind Mobile
      $(".pollmobile .titlepage").text(item.question.question_details);
      var pollmobile_1 = "<img src='"+item.option[0].image_url+"' id='"+item.option[0].id+"' />";
      var pollmobile_2 = "<img src='"+item.option[1].image_url+"' id='"+item.option[1].id+"' />";
      $(".pollmobile .bannercontent1").html(pollmobile_1);
      $(".pollmobile .bannercontent3").html(pollmobile_2);
      var animation_pollmobile = '<img class="img01" id="poll-1" src="'+item.option[0].image_url+'" style=" position: absolute;">';
          animation_pollmobile += ' <img class="img02" id=poll-2 src="'+item.option[1].image_url+'" style=" opacity: 0;  position: absolute;">';
      $(".pollmobile .animation").html(animation_pollmobile);
      $(".pollmobile .bg_text_hand").text(item.score + '% match');
      
      // Bind Comment
      document.getElementById('comments').innerHTML='';
      parser=document.getElementById('comments');
      parser.innerHTML='<div class="fb-comments triangle-obtuse left" data-width="100%" data-numposts="5" data-href="'+window.location.href+'"></div>';
      FB.XFBML.parse(parser);

      var like_count = item.option[0].votes.length;
      var dislike_count = item.option[1].votes.length;
      $(".like_count").text(like_count);
      $(".dislike_count").text(dislike_count);
      var user_id = item.user_id;
      var check_like = false, check_dislike = false;

      // Add meta
//       var image = '<meta property="og:image" content="'+item.thumbnail_url+'"/>';
//       var title = '<meta property="og:title" content="'+item.question.question_details+'"/>';
//       var url = '<meta property="og:url" content="'+"https://apps.facebook.com/butterflybeauty/" + item.id+'"/>';
//       var site_name = '<meta property="og:site_name" content="ButterflyHub"/>';
//       var type = '<meta property="og:type" content="website"/>';
//       var description = '<meta property="og:description" content="'+"Hey there, help "+ item.user_poll.username +" make better decisions with your vote. Every vote matters!" +
// " Explore millions of different beauty looks for different occasions on our Butterfly Beauty application on the AppStore. Download now!"+'"/>';
//       var app_id = '<meta content="1055854274498060" property="fb:app_id">';
//       $('head').append(image);
//       $('head').append(title);
//       $('head').append(url);
//       $('head').append(site_name);
//       $('head').append(type);
//       $('head').append(description);
//       $('head').append(app_id);

      for(i = 0; i < item.option[0].votes.length; i++)
      {
        if(user_id == item.option[0].votes[i].user_id)
        {
          check_like = true;
          $(".like_count").parent().find("input").css("font-weight","bold");
          $(".unlike_gif").css("cursor","auto");
          $(".unlike_gif").unbind("click");
          $(".like_gif").bind("click",function(event){
            Vote($(this));
          });
        }
      }
      for(i = 0; i < item.option[1].votes.length; i++)
      {
        if(user_id == item.option[1].votes[i].user_id)
        {
          check_dislike = true;
          $(".dislike_count").parent().find("input").css("font-weight","bold");
          $(".like_gif").css("cursor","auto");
          $(".like_gif").unbind("click");
          $(".unlike_gif").bind("click",function(event){
            Vote($(this));
          });
        }
      }
      if(!check_like && !check_dislike)
      {
        $(".like_gif").css("cursor","pointer");
        $(".unlike_gif").css("cursor","pointer");
        $(".like_gif").bind("click",function(event){
            Vote($(this));
          });
        $(".unlike_gif").bind("click",function(event){
            Vote($(this));
          });
      }

      $('#fb-share').bind("click",function(e){
        var data = $("#main_data").val();
        var item = JSON.parse(data);
        var description = "Hey there, help "+ item.polls[0].user_poll.username +" make better decisions with your vote. Every vote matters!" +
" Explore millions of different beauty looks for different occasions on our Butterfly Beauty application on the AppStore. Download now!";

        e.preventDefault();
        FB.ui(
        {
          method: 'feed',
          name: item.polls[0].question.question_details,
          link: "https://apps.facebook.com/butterflybeauty/" + item.polls[0].id,
          picture: item.polls[0].thumbnail_url,
          caption: "https://appsto.re/vn/pCYSdb.i",
          description: description
        });
      });
      $('#fb-share-mobile').bind("click",function(e){
        var data = $("#main_data").val();
        var item = JSON.parse(data);
        var description = "Hey there, help "+ item.polls[0].user_poll.username +" make better decisions with your vote. Every vote matters!" +
" Explore millions of different beauty looks for different occasions on our Butterfly Beauty application on the AppStore. Download now!";

        e.preventDefault();
        FB.ui(
        {
          method: 'feed',
          name: item.polls[0].question.question_details,
          link: "https://apps.facebook.com/butterflybeauty/" + item.polls[0].id,
          picture: item.polls[0].thumbnail_url,
          caption: "https://appsto.re/vn/pCYSdb.i",
          description: description
        });
      });
      LoadRelative();
    }

    function LoadRelative()
    {
      var data = $("#main_data").val();
      var item = JSON.parse(data).relative;
      var poll = "", poll_mobile = "";
      for(i = 0; i < item.length; i++)
      {
        // Bind Non-Mobile Relative
          var length_id = window.location.href.split('/')[window.location.href.split('/').length - 1].length;
          var href = "https://apps.facebook.com/butterflybeauty/" + item[i].id;
          var poll_left = "<img src='"+item[i].option[0].image_url+"' id='"+item[i].option[0].id+"' />";
          var poll_right = "<img src='"+item[i].option[1].image_url+"' id='"+item[i].option[1].id+"' />";
          if(i % 2 == 0)
          {
            poll += '<div class="item">';
            poll += '<a target="_blank" href="'+href+'">';
            poll += '<div class="col-sm-offset-1 col-sm-5 col-md-5 col-md-offset-1">';
            poll += '<div class="hand"></div>';
            poll += '<div class="col-sm-6 col-md-6 bannercontent1 no-padding">'+poll_left+'</div>';
            poll += '<div class="col-sm-6 col-md-6 bannercontent3 no-padding">'+poll_right+'</div>';
            poll += '<div class="col-sm-12 col-md-12 footertext">';
            poll += '<p>'+item[i].question.question_details+'</p>';
            poll += '</div></div></a>';
            if(i == item.length - 1)
            {
                poll += '</div>';
                $('#owl-demo').append(poll);
                poll = "";
            }
          }else
          {
            poll += '<a target="_blank" href="'+href+'">';
            poll += '<div class="col-sm-5 col-md-5">';
            poll += '<div class="hand"></div>';
            poll += '<div class="col-sm-6 col-md-6 bannercontent1 no-padding">'+poll_left+'</div>';
            poll += '<div class="col-sm-6 col-md-6 bannercontent3 no-padding">'+poll_right+'</div>';
            poll += '<div class="col-sm-12 col-md-12 footertext">';
            poll += '<p>'+item[i].question.question_details+'</p>';
            poll += '</div></div></a>';
            poll += '</div>';
            $('#owl-demo').append(poll);
            poll = "";
          }
          //Bind Mobile Relative
          poll_mobile += '<div class="item">';
          poll_mobile += '<a target="_blank" href="'+href+'">';
          poll_mobile += '<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1">';
          poll_mobile += '<div class="hand"></div>';
          poll_mobile += '<div class="col-xs-6 col-sm-6 bannercontent1 no-padding">'+poll_left+'</div>';
          poll_mobile += '<div class="col-xs-6 col-sm-6 bannercontent3 no-padding">'+poll_right+'</div>';
          poll_mobile += '<div class="col-xs-12 col-sm-12 footertext">';
          poll_mobile += '<p class="col-xs-12 col-sm-12">'+item[i].question.question_details+'</p>';
          poll_mobile += '</div></div></a></div>';
      }
        var owl = $("#owl-demo");
        owl.owlCarousel({
          items:1,
          itemsDesktop : [1000,1], //5 items between 1000px and 901px
          itemsDesktopSmall : [900,1], // betweem 900px and 601px
          itemsTablet: [600,1], //2 items between 600 and 0
          itemsMobile : false ,// itemsMobile disabled - inherit from itemsTablet option
          pagination: false,
          navigation : true,
          autoPlay: true,
          navigationText: [
            "<image src='../img/prev.png' />",
            "<image src='../img/next.png' />"
          ]
        });
        $('#owl-demo-mobile').html(poll_mobile);
        var owl_mobile = $("#owl-demo-mobile");
        owl_mobile.owlCarousel({
            singleItem: true,
            pagination: false,
            navigation : true,
            autoPlay: true,
            navigationText: [
              "<image src='../img/prev.png' />",
              "<image src='../img/next.png' />"
            ]
        });
    }

    function Vote(item)
    {
      var isLike = $(item).hasClass('like_gif');
      if(isLike)
      {
        var checkMobile = $(item).parent().parent().parent().hasClass('likepagemobile');
        if(!checkMobile)
        {
          var html  = "<img src='gif/like.gif'/>";
            $(item).html(html);
            setTimeout(function () {
              $(item).html('');
            },3200);
        }
        $(".unlike_gif").css("cursor","auto");
        $(".unlike_gif").unbind("click");
        var poll_option_id = $(".bannercontent1").find("img").attr("id");
        var poll_id = window.location.pathname.split('/')[1];
        $.ajax({
          type: "POST",
          url: "/vote",
          contentType: "application/json",
          data: JSON.stringify({"poll_option_id" : poll_option_id, "poll_id" : poll_id}) ,
          dataType: 'json',
          async: false,
          success: function (data) {
            if(data.result == "success")
            {
              $(".like_gif").unbind("click");
              $(".unlike_gif").unbind("click");
              $(".like_count").parent().find("input").css("font-weight","normal");
              $(".dislike_count").parent().find("input").css("font-weight","normal");
              $("#owl-demo").data('owlCarousel').destroy();
              $("#owl-demo-mobile").data('owlCarousel').destroy();
              Init();
            }
          },
          error: function (error) {
              alert(error.status + "<--and--> " + error.statusText);
            }
        });  
      }else
      {
        var checkMobile = $(item).parent().parent().parent().hasClass('likepagemobile');
        if(!checkMobile)
        {
          var html  = "<img src='gif/unlike.gif'/>";
          $(item).html(html);
          setTimeout(function () {
            $(item).html('');
          },5450);
        }
        $(".like_gif").css("cursor","auto");
        $(".like_gif").unbind("click");
        var poll_option_id = $(".bannercontent3").find("img").attr("id");
        var poll_id = window.location.pathname.split('/')[1];
        $.ajax({
          type: "POST",
          url: "/vote",
          contentType: "application/json",
          data: JSON.stringify({"poll_option_id" : poll_option_id, "poll_id" : poll_id}) ,
          dataType: 'json',
          async: false,
          success: function (data) {
            if(data.result == "success")
            {
              $(".like_gif").unbind("click");
              $(".unlike_gif").unbind("click");
              $(".like_count").parent().find("input").css("font-weight","normal");
              $(".dislike_count").parent().find("input").css("font-weight","normal");
              Init();
            }
          },
          error: function (error) {
              alert(error.status + "<--and--> " + error.statusText);
            }
        }); 
      }
    }
  };
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));