FROM 407156075963.dkr.ecr.us-west-2.amazonaws.com/facebook_app:latest

RUN mkdir -p /opt/butterflyhub/facebook-app
COPY . /opt/butterflyhub/facebook-app/

WORKDIR /opt/butterflyhub/facebook-app
RUN npm install

CMD ["node", "./bin/www"]
