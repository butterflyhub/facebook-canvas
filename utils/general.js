var env       = process.env.NODE_ENV || "development";

const config = require(__dirname + '/../config/config.json');
const api_server = config["backend_url"][env];
const host_url = config["host_url"][env];
module.exports = Object.freeze({
    env: env,
    config: config,
    api_server: api_server,
    host_url: host_url
});